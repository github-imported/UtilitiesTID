# UtilitiesTID
### Required .NET Desktop Runtime 6.0.3 or SDK 6.0.3
<p>Set of useful tools for my work as a Clarion developer, designed to work on Windows.</p>

- Clarion Conversion
	- Convert time and date values to clarion standard.

- Code Clipboard
	- Store repetitive code in a simple SQLite Database.
	- Copy already registered codes to the clipboard.

- File Management
	- Generate or validade hash of files (Checksum). 
	- Determine which process and user is using determined folder or file.
	- Watch for actions or changes on files or folders.
	- Copy, Move or delete files easily with custom filters.

- Process Management
	- Get information on all running processes
<br>
<img src="https://gitlab.com/Nekrosu/UtilitiesTID/-/raw/9eb74b99824f5b56042ea9aaab79065d7f730388/UtilitiesTID.png">
